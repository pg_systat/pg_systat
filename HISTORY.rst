Release Notes
=============

2023-07-01 v1.1.0
-----------------

* Add Docker files for creating an AppImage
* Fixed bug where CFLAGS was being overridden
* Split manpage generation into separate CMakeLists.txt
* Use Podman for containers instead of Docker
* Replace reallocarray() with realloc()
* Add additional views: buffercachestat, buffercacherel, copyprogress, stmtwal,
  stmttempblk, stmtlocalblk, stmtsharedblk, stmtexec, stmtplan
* index view: show change in stats
* indexio view: show change in stats
* tableanalyze: adjust column sizes
* tableioheap: adjust column sizes
* tableioidx: adjust column sizes
* tableiotidx: adjust column sizes
* tableiotoast: adjust column sizes
* tablescan: adjust column sizes
* tabletup: adjust column sizes
* tablevac: adjust column sizes
* vacuum: adjust column sizes
* dbblk: adjust column sizes

2020-10-08 v1.0.0
-----------------

* Initial release that monitors database, table, index, tablespace, vacuum, and
  standby statistics.
